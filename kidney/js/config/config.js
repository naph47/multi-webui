var USER_CONFIG = {
    FILE_PATH: {
        ROOT: "./dydata/",
        CHECK_EXIST: "healthdata.csv",
        FOOD: ["foodrec-s1to4.txt", "foodrec-s5.txt"],
        EXERCISE: ["exercise.txt"],
        HEALTH_DATA: "healthdata.csv",
    }
}

var BE_URL = "http://52.72.175.132:8080/fid-kidney";

var QUERIES = {
    SIMPLE_RESULT: 'Find.Result',
    TYPE: 'SIMPLE',
    OBJECT_GFR_DATA: 'GFRData',
    LOAD_GFR_DATA: '<Query><Find><GFRData><UserId eq="{0}" /></GFRData></Find></Query>',
    SAMPLE_RESPONSE: '<Result>\n' +
    '        <GFRData>\n' +
    '            <GFRDataId></SysId>\n' +
    '            <Age></Age>\n' +
    '            <SerumCreatinine></SerumCreatinine>\n' +
    '            <Gender></Gender>\n' +
    '            <African>true/false</African>\n' +
    '            <Result></Result>\n' +
    '            <Date></Date>\n' +
    '        </GFRData>\n' +
    '    </Result>',
    CALCULATE: '<CalculateGFR><UserId>{0}</UserId><Age>{1}</Age><SerumCreatinine>{2}</SerumCreatinine><Gender>{3}</Gender><African>{4}</African></CalculateGFR>',
}