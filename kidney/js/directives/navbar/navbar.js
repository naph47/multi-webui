'use strict';

/**
 * @ngdoc directive
 * @name greenhouseuiApp.directive:navBar/navBar
 * @description
 * # navBar/navBar
 */
angular.module('sampleUIApp')
    .directive('navBarTop', function ($location, $timeout, $rootScope) {
        return {
            templateUrl: "./js/directives/navbar/navbar.html",
            restrict: 'E',
            link: function postLink(scope, element, attrs) {
                scope.isState = function (name) {
                    return name == $location.path();
                }

                scope.goTo = function (name) {
                    $location.path(name);
                }


            }
        };
    });
