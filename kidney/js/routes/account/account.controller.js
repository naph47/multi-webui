'use strict';

angular.module('sampleUIApp')
    .controller('AccountCtrl', function ($scope, $location, $timeout, $rootScope, UserService) {
        var vm = this;

        vm.logout = function () {
            $rootScope.currentUser = null;
            localStorage.removeItem("current-user");
            $location.path("/home");
        }

        vm.login = function () {
            vm.errorMessage = null;
            UserService.login(vm.username, vm.password).then(function (rs) {
                if (rs === true) {
                    //    logined
                    checkLogin();
                } else {
                    console.log(rs);
                    vm.errorMessage = rs;
                }
            });
        }

        function checkLogin() {
            return $timeout(function () {
                if (localStorage.getItem("current-user") && localStorage.getItem("current-user") !== "null") {
                    $rootScope.currentUser = JSON.parse(localStorage.getItem("current-user"));
                    return true;
                } else {
                    return false;
                }
            }).then(function (rs) {
                if (rs) {
                    UserService.loadFoodReceipt($rootScope.currentUser.username).then(function (rs) {
                        document.getElementById('food-receipt-content').textContent = rs;
                    });
                    UserService.loadExercise($rootScope.currentUser.username).then(function (rs) {
                        document.getElementById('exercise-content').textContent = rs;
                    });
                    UserService.loadHealthData($rootScope.currentUser.username).then(function (rs) {
                        console.log(rs);
                        vm.historyHealthData = rs;
                    });

                }
            });

        }

        $timeout(function () {
            checkLogin();
        })
    });
