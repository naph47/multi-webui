'use strict';

angular.module('sampleUIApp')
    .controller('GFRCalculatorCtrl', function ($scope, $location, $timeout, ApiService, $rootScope) {
        var vm = this;
        vm.ages = [];

        vm.loadGFRData = function () {
            return ApiService.getGFRData($rootScope.currentUser.username).then(function (rs) {
                console.log(rs);
                vm.data = rs;
            })
        }
        vm.calculateGFRData = function () {
            return ApiService.calculateGFRData($rootScope.currentUser.username, vm.age, vm.secrum, vm.gender, vm.africa).then(function (rs) {
                console.log(rs);
                if (rs[0].GFRValue._Value)
                    vm.result = rs[0].GFRValue._Value;
                vm.loadGFRData();
            })
        }
        $timeout(function () {
            for (var i = 12; i < 100; i++) {
                vm.ages.push(i);
            }
            if (localStorage.getItem("current-user") && localStorage.getItem("current-user") !== "null") {
                $rootScope.currentUser = JSON.parse(localStorage.getItem("current-user"));
            }
            vm.loadGFRData();

        })
    });
