'use strict';

angular.module('sampleUIApp')
    .service('UserService', ['$rootScope', '$timeout', '$http', 'GeneralService', function ($rootScope, $timeout, $http, GeneralService) {
        var self = this;
        self.login = function (username, password) {
            return checkExistUser(username).then(function (isExisted) {
                if (isExisted) {
                    var user = {
                        username: username,
                        password: password
                    };
                    $rootScope.currentUser = user;
                    localStorage.setItem("current-user", JSON.stringify(user));
                    $('html').scrollTop(0);
                    return true;
                } else {
                    return "User is not existed";
                }
            })
        }

        self.loadFoodReceipt = function (username) {
            var data = "";
            return $http.get(USER_CONFIG.FILE_PATH.ROOT + username + "/" + USER_CONFIG.FILE_PATH.FOOD[0]).then(function (rs) {
                data += rs.data;
                return $http.get(USER_CONFIG.FILE_PATH.ROOT + username + "/" + USER_CONFIG.FILE_PATH.FOOD[1]).then(function (rs) {
                    data += rs.data;
                    return data;
                })
            });
        }
        self.loadExercise = function (username) {
            var data = "";
            return $http.get(USER_CONFIG.FILE_PATH.ROOT + username + "/" + USER_CONFIG.FILE_PATH.EXERCISE).then(function (rs) {
                data += rs.data;
                return data;
            });
        }

        self.loadHealthData = function (username) {
            return $http.get(USER_CONFIG.FILE_PATH.ROOT + username + "/" + USER_CONFIG.FILE_PATH.HEALTH_DATA).then(function (rs) {
                return rs.data.split(",");
            })
        };

        function checkExistUser(username) {
            return $http.get(USER_CONFIG.FILE_PATH.ROOT + username + "/" + USER_CONFIG.FILE_PATH.CHECK_EXIST).then(function (rs) {
                return true;
            }, function (e) {
                console.log(e);
                return false;
            });
        }

        return self;
    }])