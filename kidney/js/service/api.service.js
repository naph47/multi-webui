'use strict';

angular.module('sampleUIApp')
    .service('ApiService', ['$http', '$q', 'GeneralService', '$timeout', function ($http, $q, GeneralService, $timeout) {


        var self = this;

        self.$timeout = $timeout;

        function simpleApiCall(req) {
            return $http(req);
        }

        function simpleApiCallWS(url, query, isStop, callback) {

            var x2js = new X2JS();

            /*open a websocket for the force sensor*/
            var fws = new WebSocket(url);
            fws.onopen = function () {
                // Web Socket is connected, send data using send()
                //console.log("ws open");
                fws.send(query);
            };

            fws.onmessage = function (e) {
                var server_message = e.data;

                if (callback) {
                    callback(server_message);
                    if (isStop)
                        fws.close();
                }
            };
            fws.onerror = function (error) {
                console.log('Error detected: ' + error);
            };
            fws.onclose = function () {
                console.log("ws close");
            };

            return fws;
        };


        function apiCall(req, path, object, type, params, includeRawData) {
            if (params)
                for (var i = 0; i < params.length; i++) {
                    req.data = GeneralService.replaceAll(req.data, '{' + i + '}', params[i]);
                }

            return $http(req).then(function (response) {

                var x2js = new X2JS();

                var json = x2js.xml_str2json(response.data);

                //check if json is null
                if (json == null) {
                    json = x2js.xml_str2json("<data>" + response.data + "</data>");
                }

                if (path) {
                    var pathArray = path.split('.');
                    for (var i = 0; i < pathArray.length; i++) {
                        json = json[pathArray[i]];
                    }
                }

                if (typeof json === 'undefined') {
                    return [];
                }

                if (!angular.isArray(json)) {
                    json = [json];
                }

                //remove Object

                if (object)
                    if (type == "SIMPLE") {
                        for (var i = 0; i < json.length; i++) {
                            if (typeof json[i] !== 'undefined') {
                                json[i] = json[i][object];
                            } else {

                            }
                        }
                    }

                if (includeRawData) {
                    json = {
                        data: json,
                        rawData: response.data
                    };
                }


                return json;
            });
        };

        function apiCallWS(url, query, callback) {

            var x2js = new X2JS();

            /*open a websocket for the force sensor*/
            var fws = new WebSocket(url);
            fws.onopen = function () {
                // Web Socket is connected, send data using send()
                //console.log("ws open");
                fws.send(query);
            };

            fws.onmessage = function (e) {
                var server_message = e.data;

                var tagArray = server_message.split(/[<>]/);

                for (var i = 0; i < tagArray.length; i++) {
                    if (tagArray[i].trim().indexOf(" ") >= 0 && tagArray[i].trim().indexOf("Equipment") === 0) {
                        var replaceS = GeneralService.replaceAll(tagArray[i], " ", "-");
                        server_message = GeneralService.replaceAll(server_message, tagArray[i], replaceS);
                    }
                }


                var jsonObj = x2js.xml_str2json(server_message);
                var result = (jsonObj && typeof jsonObj.TqlNotification !== 'undefined') ? jsonObj.TqlNotification.Update : 0;
                if (!result) {
                    result = (jsonObj && typeof jsonObj.TqlNotification !== 'undefined') ? jsonObj.TqlNotification.Create : 0;
                }
                if (callback) {
                    if (typeof result !== 'undefined' && result) {
                        var key = Object.keys(result)[0];
                        var changeKey = GeneralService.replaceAll(key, "-", " ");
                        var obj = {};
                        obj[changeKey] = result[key];
                        callback(obj);
                    }
                    // else {
                    //     callback(0);
                    // }
                }
            };
            fws.onerror = function (error) {
                console.log('Error detected: ' + error);
            };
            fws.onclose = function () {
                console.log("ws close");
            };

            return fws;
        };

        self.getGFRData = function (user) {
            var req = {
                method: 'POST',
                url: BE_URL,
                data: QUERIES.LOAD_GFR_DATA
            };
            return apiCall(req, QUERIES.SIMPLE_RESULT, QUERIES.OBJECT_GFR_DATA, QUERIES.TYPE, [user]);
        };
        self.calculateGFRData = function (user, age, secrum, gender, africa) {
            var req = {
                method: 'POST',
                url: BE_URL,
                data: QUERIES.CALCULATE
            };
            return apiCall(req, "Create", QUERIES.OBJECT_GFR_DATA, QUERIES.TYPE, [user, age, secrum, gender, africa]);
        };
        return self;
    }]);
