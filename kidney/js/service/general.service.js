'use strict';

angular.module('sampleUIApp')
    .service('GeneralService', ['$rootScope', function ($rootScope) {
        var WS_STORAGE = {};
        var self = this;

        function escapeRegExp(str) {
            return str.replace(/([.*+?^=!:${}()|\[\]\/\\])/g, "\\$1");
        };

        self.replaceAll = function (str, find, replace) {
            return str.replace(new RegExp(escapeRegExp(find), 'g'), replace);
        };

        self.csvJSON = function (csv) {

            var lines = csv.split("\n");

            var result = [];

            var headers = lines[0].split(",");

            for (var i = 1; i < lines.length; i++) {

                var obj = {};
                var currentline = lines[i].split(",");

                for (var j = 0; j < headers.length; j++) {
                    obj[headers[j]] = currentline[j];
                }

                result.push(obj);

            }

            //return result; //JavaScript object
            return JSON.stringify(result); //JSON
        }

        self.triggerSelect = function () {
            //select
            $(".selectpicker").selectpicker("refresh");

        };

        self.scrollToCurrentTimeLine = function (div) {
            var $currentTL = $(".current-time-line");

            angular.element(div).scrollLeft($currentTL.position().left - angular.element(div).width() / 2);
        };

        self.convertLocalToUtc = function (timestamp) {
            return new Date().toUTCString();
        };

        self.convertUTCToLocal = function (timestamp) {
            return moment.utc(timestamp).toDate();
        };

        self.isEmptyObject = function (obj) {
            for (var key in obj) {
                if (obj.hasOwnProperty(key))
                    return false;
            }
            return true;
        }

        self.removeHourMinuteSecondMili = function (timestamp) {
            var date = new Date(timestamp);
            var currHr = date.getHours();
            var currMn = date.getMinutes();
            var currSc = date.getSeconds();
            var currMl = date.getMilliseconds();

            return timestamp - currHr * (1000 * 60 * 60) - currMn * (1000 * 60) - currSc * 1000 - currMl;
        };

        self.removeSecondMili = function (timestamp) {
            var date = new Date(timestamp);
            var currSc = date.getSeconds();
            var currMl = date.getMilliseconds();

            return timestamp - currSc * 1000 - currMl;
        };

        self.roundHalfHour = function (timestamp) {

            var ROUNDING = 30 * 60 * 1000;
            /*ms*/
            var start = moment(timestamp);
            start = moment(Math.round((+start) / ROUNDING) * ROUNDING);

            return start.toDate().getTime();
        };

        self.appendNestedData = function (obj, key) {

        }

        self.massageStaticDynamicData = function (data) {
            var attributes = $rootScope.ATTRIBUTES;
            // console.log(attributes);
            var result = {
                static: {},
                dynamic: {}
            };

            for (var key in data) {
                var foundAttribute = angular.copy(_.find(attributes, function (el) {
                    return el.attributeName === key || el.attributeName.indexOf(key + ".") == 0;
                }));

                if (typeof foundAttribute !== 'undefined') {
                    if (foundAttribute.isStatic === "true") {

                        if (foundAttribute.attributeName.indexOf(".") >= 0) {
                            //if nested
                            result.static[foundAttribute.attributeName] = foundAttribute || {};
                            result.static[foundAttribute.attributeName].value = getDescendantProp(data, foundAttribute.attributeName);
                        } else {
                            result.static[key] = foundAttribute || {};
                            result.static[key].value = data[key];
                        }
                    } else {
                        if (foundAttribute.attributeName.indexOf(".") >= 0) {
                            //if nested
                            result.dynamic[foundAttribute.attributeName] = foundAttribute || {};
                            result.dynamic[foundAttribute.attributeName].value = getDescendantProp(data, foundAttribute.attributeName);
                        } else {
                            result.dynamic[key] = foundAttribute || {};
                            result.dynamic[key].value = data[key];
                        }

                    }
                }
            }

            function getDescendantProp(obj, desc) {
                var arr = desc.split('.');
                while (arr.length && (obj = obj[arr.shift()])) ;
                return obj;
            }

            // filter by assetType
            if (typeof data.assetType !== 'undefined') {
                var typeFilter = angular.copy(_.filter(attributes, {entityType: data.assetType}));
                for (var i = 0; i < typeFilter.length; i++) {
                    if (typeof result.static[typeFilter[i].attributeName] !== 'undefined') {
                        result.static[typeFilter[i].attributeName].label = typeFilter[i].label;
                    }
                    if (typeof result.dynamic[typeFilter[i].attributeName] !== 'undefined') {
                        result.dynamic[typeFilter[i].attributeName].label = typeFilter[i].label;
                    }
                }
            }
            // console.log(result);

            return angular.copy(result);
        };

        self.getAttributeDetails = function (key) {
            return _.find($rootScope.ATTRIBUTES, {attributeName: key});
        };

        self.addNewWS = function (key, data, callback) {
            WS_STORAGE[key] = {};
            WS_STORAGE[key].data = data;
            WS_STORAGE[key].callback = callback;
            //console.log(WS_STORAGE);
        };

        self.getWSById = function (key) {
            return WS_STORAGE[key];
        };

        self.updateWSById = function (key, subKey, value) {
            WS_STORAGE[key][subKey] = value;
        }

        self.toDegrees = function (angle) {
            return angle * (180 / Math.PI);
        }

        self.addMinutes = function (time, minute) {
            return moment(time).add(parseInt(minute), 'minutes');
        }

        self.generateDateString = function (startDate, postfix) {
            if (!postfix) {
                postfix = "";
            }
            var dateArray = "";
            /* for (var i = 0; i < 4; i++) {
             var date = new Date(startDate);
             date.setMilliseconds(0);
             date.setHours(0);
             date.setMinutes(0);
             console.log(date.toISOString());
             //dateArray += date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate() + postfix + ',';
             dateArray+=date.toISOString()+',';
             startDate += 24 * 60 * 60 * 1000;
             }*/
            var date = new Date(startDate);
            dateArray += date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate() + "T00:00:00.000Z" + ',';
            startDate += 96 * 60 * 60 * 1000;
            //4 days
            date = new Date(startDate);
            dateArray += date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate() + "T23:59:00.000Z";
            //  dateArray = dateArray.substring(0, dateArray.lastIndexOf(','));
            return dateArray;
        };
    }])