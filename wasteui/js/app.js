angular
    .module('sampleUIApp', [
        'ngAnimate',
        'ngCookies',
        'ngResource',
        'ngRoute',
        'ngSanitize',
        'ngTouch',
        'ngMaterial',
        'ngMessages',
        'ui.codemirror',
        'duScroll',
        'angular-loading-bar'
    ])
    .config(function ($routeProvider, cfpLoadingBarProvider) {

        cfpLoadingBarProvider.includeSpinner = false;

        $routeProvider
            .when('/', {
                redirectTo: '/resident'
            })
            .when('/resident', {
                templateUrl: 'js/routes/resident/resident.html',
                controller: 'ResidentCtrl',
                controllerAs: 'vm',
                resolve: {}
            })
            .when('/performance', {
                templateUrl: 'js/routes/performance/performance.html',
                controller: 'PerformanceCtrl',
                controllerAs: 'vm',
                resolve: {}
            })
            .when('/posts', {
                templateUrl: 'js/routes/posts/posts.html',
                controller: 'PostsCtrl',
                controllerAs: 'vm',
                resolve: {}
            })
            .when('/trashbin', {
                templateUrl: 'js/routes/trashbin/trashbin.html',
                controller: 'TrashbinCtrl',
                controllerAs: 'vm',
                resolve: {}
            })
            .when('/dashboard', {
                templateUrl: 'js/routes/dashboard/dashboard.html',
                controller: 'DashboardCtrl',
                controllerAs: 'vm',
                resolve: {}
            })
            .otherwise({
                redirectTo: '/'
            });
    });
