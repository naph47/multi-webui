'use strict';

angular.module('sampleUIApp')
    .controller('ResidentCtrl', function ($scope, $location, $timeout, QueryService) {

        var vm = this;

        vm.findAllResident = function () {
            vm.residents = [];
            QueryService.findAllResident(function (rs) {
                console.log(rs);
                $timeout(function () {
                    vm.residents = rs;
                });
            });
        };

        $timeout(function () {
            vm.findAllResident();
            QueryService.WebSocketTest();
        });
    });
