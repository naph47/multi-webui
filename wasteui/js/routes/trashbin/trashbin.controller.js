'use strict';

angular.module('sampleUIApp')
    .controller('TrashbinCtrl', function ($scope, $location, $timeout, QueryService) {

        var vm = this;

        vm.findTrashBin = function () {
            vm.itemsTrashBin = [];
            findTrashBin(function (rs) {
                console.log(rs);
                $timeout(function () {
                    vm.itemsTrashBin = rs;
                })
            });
        };
        vm.findAllCompany = findAllCompany;

        var broken = false;

        function callQuery(params, call, callback) {

            var http = new XMLHttpRequest();
            var url = TQL_URL;
            http.open("POST", url, true);
            http.setRequestHeader("Content-Type", "text/xml");
            //Send the proper header information along with the request

            //http.setRequestHeader("Content-length", params.length);
            //http.setRequestHeader("Connection", "close");

            http.onreadystatechange = function () {//Call a function when the state changes.
                if (http.readyState == 4 && http.status == 200) {
                    //alert(http.responseText);
                    // var div = document.getElementById("dd");


                    // div.innerHTML = "";
                    var parser = new DOMParser();
                    var xmlDoc = parser.parseFromString(http.responseText, "text/xml");
                    var jsonDoc = JSON.parse(xml2json(xmlDoc, ""));
                    var trashbin_arr = _.pluck(jsonDoc.Find.Result, "TrashBin");
                    var array = [];

                    for (var k = 0; k < trashbin_arr.length; k++) {
                        var trashBinId = trashbin_arr[k].Id;
                        var iBeacon = trashbin_arr[k].HasIBeacon["@Value"];

                        if (iBeacon == "false") {
                            var has_ibeacon = "No";
                        } else {
                            var has_ibeacon = "yes";
                        }

                        var capacity = trashbin_arr[k].Capacity["@Value"];
                        var cashInsentive = trashbin_arr[k].CashIncentive["@Value"];
                        var hasRF = trashbin_arr[k].HasRF["@Value"];

                        if (hasRF == "false") {
                            var has_RF = "No";
                        } else {
                            var has_RF = "yes";
                        }

                        array.push({
                            trashBinId: trashBinId,
                            has_ibeacon: has_ibeacon,
                            capacity: capacity,
                            cashInsentive: cashInsentive,
                            hasRF: hasRF
                        })


                        // var table = document.getElementById("myTable");
                        // var row = table.insertRow(-1);
                        //
                        // var cell1 = row.insertCell(-1);
                        // cell1.innerHTML = trashBinId;
                        //
                        // var cell2 = row.insertCell(1);
                        // cell2.innerHTML = has_ibeacon;
                        //
                        // var cell3 = row.insertCell(2);
                        // cell3.innerHTML = capacity;
                        //
                        // var cell4 = row.insertCell(3);
                        // cell4.innerHTML = cashInsentive;
                        //
                        // var cell5 = row.insertCell(4);
                        // cell5.innerHTML = has_RF;


                    }

                    if (callback) {
                        callback(array);
                    }
                }
            }
            http.send(params);

        }

        function findTrashBin(callback) {
            callQuery("<find format='all'><TrashBin><Id ne=''/></TrashBin></find>", "all", callback);
        }

        document.getElementById("clock").onload = function () {
            WebSocketTest()

        };

        function sendMsg() {
            console.log("message sent");
        }

        function findResidentByID(id) {
            document.getElementById("currentPedestrian").innerHTML = id;
            callQuery("<find format='all'><Resident><Id eq='" + id + "'/></Resident></find>", "getCurrent");
        }


        function findWasteBin() {

            callQuery2(find_TempSensor, "all");
            callQuery3(find_HumiditySensor, "all");
            callQuery4(find_AmbianceSensor, "all");
        }

        function callQuery2(params, call) {

            var http = new XMLHttpRequest();
            var url = "http://1.1.1.1:8080/fid-iotkitthings";
            http.open("POST", url, true);
            http.setRequestHeader("Content-Type", "text/xml");
            //Send the proper header information along with the request

            //http.setRequestHeader("Content-length", params.length);
            //http.setRequestHeader("Connection", "close");

            http.onreadystatechange = function () {//Call a function when the state changes.
                if (http.readyState == 4 && http.status == 200) {
                    //alert(http.responseText);
                    var div = document.getElementById("waste");
                    div.innerHTML = "";
                    var parser = new DOMParser();
                    var xmlDoc = parser.parseFromString(http.responseText, "text/xml");
                    var jsonDoc = JSON.parse(xml2json(xmlDoc, ""));
                    var tempsensor_arr = _.pluck(jsonDoc.Find.Result, "TempSensorModel");

                    console.log(tempsensor_arr[0]);

                    for (var k = 0; k < tempsensor_arr.length; k++) {
                        //var tempsensorID =  tempsensor_arr[k].TempSensorId;
                        var tempvalueinF = tempsensor_arr[k].TempValueInF["@Known"];


                        var table = document.getElementById("myWasteBin");
                        var row = table.insertRow(-1);

                        var cell1 = row.insertCell(-1);
                        cell1.innerHTML = tempvalueinF;

                    }
                }
            }
            http.send(params);

        }

        function callQuery3(params, call) {

            var http = new XMLHttpRequest();
            var url = "http://1.1.1.1:8080/fid-iotkitthings";
            http.open("POST", url, true);
            http.setRequestHeader("Content-Type", "text/xml");
            //Send the proper header information along with the request

            //http.setRequestHeader("Content-length", params.length);
            //http.setRequestHeader("Connection", "close");

            http.onreadystatechange = function () {//Call a function when the state changes.
                if (http.readyState == 4 && http.status == 200) {
                    //alert(http.responseText);
                    var div = document.getElementById("waste");
                    div.innerHTML = "";
                    var parser = new DOMParser();
                    var xmlDoc = parser.parseFromString(http.responseText, "text/xml");
                    var jsonDoc = JSON.parse(xml2json(xmlDoc, ""));
                    var humiditysensor_arr = _.pluck(jsonDoc.Find.Result, "HumiditySensorModel");

                    console.log(humiditysensor_arr[0]);

                    for (var k = 0; k < humiditysensor_arr.length; k++) {
                        //var humiditysensorID =  humiditysensor_arr[k].HumiditySensorId;
                        var humsensor = humiditysensor_arr[k].HumSensor["@Known"];


                        var table = document.getElementById("myWasteBin");
                        var row = table.insertRow(-1);

                        var cell1 = row.insertCell(-1);
                        cell1.innerHTML = humsensor;

                    }
                }
            }
            http.send(params);

        }

        function callQuery4(params, call) {

            var http = new XMLHttpRequest();
            var url = "http://1.1.1.1:8080/fid-iotkitthings";
            http.open("POST", url, true);
            http.setRequestHeader("Content-Type", "text/xml");
            //Send the proper header information along with the request

            //http.setRequestHeader("Content-length", params.length);
            //http.setRequestHeader("Connection", "close");

            http.onreadystatechange = function () {//Call a function when the state changes.
                if (http.readyState == 4 && http.status == 200) {
                    //alert(http.responseText);
                    var div = document.getElementById("waste");
                    div.innerHTML = "";
                    var parser = new DOMParser();
                    var xmlDoc = parser.parseFromString(http.responseText, "text/xml");
                    var jsonDoc = JSON.parse(xml2json(xmlDoc, ""));
                    var ambiancesensor_arr = _.pluck(jsonDoc.Find.Result, "AmbianceSensorModel");

                    console.log(ambiancesensor_arr[0]);

                    for (var k = 0; k < ambiancesensor_arr.length; k++) {
                        //var ambiancesensorID =  ambiancesensor_arr[k].AmbianceSensorId;
                        var ambsensor = ambiancesensor_arr[k].AmbSensor["@Known"];


                        var table = document.getElementById("myWasteBin");
                        var row = table.insertRow(-1);

                        var cell1 = row.insertCell(-1);
                        cell1.innerHTML = humsensor;

                    }
                }
            }
            http.send(params);

        }

        function callQuery5(params, call) {
            var http = new XMLHttpRequest();
            var url = TQL_URL;
            http.open("POST", url, true);
            http.setRequestHeader("Content-Type", "text/xml");
            //Send the proper header information along with the request

            //http.setRequestHeader("Content-length", params.length);
            //http.setRequestHeader("Connection", "close");

            http.onreadystatechange = function () {//Call a function when the state changes.
                if (http.readyState == 4 && http.status == 200) {
                    //alert(http.responseText);
                    var div = document.getElementById("dd");
                    div.innerHTML = "";
                    var parser = new DOMParser();
                    var xmlDoc = parser.parseFromString(http.responseText, "text/xml");
                    var jsonDoc = JSON.parse(xml2json(xmlDoc, ""));
                    var trashArr_arr = _.pluck(jsonDoc.Find.Result, "TrashBin");
                    var count1 = 0;
                    var count2 = 0;
                    for (var k = 0; k < trashArr_arr.length; k++) {

                        var hasIbeacon = trashArr_arr[k].HasIBeacon["@Value"];
                        var hasRf = trashArr_arr[k].HasRF["@Value"];

                        if (hasIbeacon == "true") {
                            count1 = count1 + 1;
                        }

                        if (hasRf == "true") {
                            count2 = count2 + 1;
                        }

                    }


                    var barChartData = {
                        labels: ["IBeacon", "Rf Sensor"],
                        datasets: [
                            {
                                fillColor: "rgba(0,255,0,0.3)",
                                strokeColor: "rgba(0,255,0,0.3)",
                                highlightFill: "rgba(0,0,255,0.3)",
                                highlightStroke: "rgba(220,220,220,1)",
                                data: [count1, count2]
                            },

                        ]

                    }
                    var ctx = document.getElementById("canvas").getContext("2d");
                    window.myBar = new Chart(ctx).Bar(barChartData, {
                        responsive: true
                    });

                }
            }
            http.send(params);

        }

        function findAllCompany() {
            callQuery5("<find format='all'><TrashBin><Id ne=''/></TrashBin></find>", "all");
        }


        $timeout(function () {
            document.getElementById("clock").onload = function () {
                WebSocketTest()
            };
            vm.findTrashBin();
            vm.findAllCompany();
        });
    });
