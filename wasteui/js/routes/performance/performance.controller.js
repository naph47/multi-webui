'use strict';

angular.module('sampleUIApp')
    .controller('PerformanceCtrl', function ($scope, $location, $timeout, QueryService) {

        var vm = this;

        vm.findAllCompany = findAllCompany;

        var broken = false;

        function callQuery(params, call, callback) {
            var http = new XMLHttpRequest();
            var url = TQL_URL;
            http.open("POST", url, true);
            http.setRequestHeader("Content-Type", "text/xml");
            //Send the proper header information along with the request

            //http.setRequestHeader("Content-length", params.length);
            //http.setRequestHeader("Connection", "close");

            http.onreadystatechange = function () {//Call a function when the state changes.
                if (http.readyState == 4 && http.status == 200) {
                    //alert(http.responseText);
                    var div = document.getElementById("dd");
                    div.innerHTML = "";
                    var parser = new DOMParser();
                    var xmlDoc = parser.parseFromString(http.responseText, "text/xml");
                    var jsonDoc = JSON.parse(xml2json(xmlDoc, ""));
                    var revenue = jsonDoc.Find.Result.Company.Revenue["@Value"];
                    var WasteVol = jsonDoc.Find.Result.Company.WasteVol["@Value"];
                    var AccountBalance = jsonDoc.Find.Result.Company.AccountBalance["@Value"];
                    var LifetimeRevenue = jsonDoc.Find.Result.Company.LifetimeRevenue["@Value"];
                    var LifetimeWasteVol = jsonDoc.Find.Result.Company.LifetimeWasteVol["@Value"];

                    var obj = {
                        revenue: revenue,
                        wasteVol: WasteVol,
                        accountBalance: AccountBalance,
                        lifetimeRevenue: LifetimeRevenue,
                        lifetimeWasteVol: LifetimeWasteVol
                    };


                    var barChartData = {
                        labels: ["revenue", "WasteVol", "Acc.Balance", "LT.Revenue", "L.TWasteVol"],
                        datasets: [
                            {
                                fillColor: "rgba(0,255,0,0.3)",
                                strokeColor: "rgba(0,255,0,0.3)",
                                highlightFill: "rgba(0,0,255,0.3)",
                                highlightStroke: "rgba(220,220,220,1)",
                                data: [revenue, WasteVol, AccountBalance, LifetimeRevenue, LifetimeWasteVol]
                            },

                        ]

                    }
                    var ctx = document.getElementById("canvas").getContext("2d");
                    window.myBar = new Chart(ctx).Bar(barChartData, {
                        responsive: true
                    });
                    var data = xmlDoc.getElementsByTagName("Company");
                    for (var i = 0; i < data.length; i++) {
                        var x = data[i].childNodes;
                        //alert(x.length);
                        for (var j = 0; j < x.length; j++) {
                            //alert(x[j]);
                            if (x[j].nodeName != '#text' && x[j].nodeName != 'Id') {
                                if (x[j].getAttribute("Value") != null) {
                                    var node = document.createElement("Div");
                                    div.appendChild(node);
                                    node.innerHTML = "<b>" + x[j].nodeName + ":</b>&nbsp;&nbsp;" + x[j].getAttribute("Value");
                                    obj[x[j].nodeName] = x[j].getAttribute("Value");
                                }


                            } else if (x[j].nodeName == 'Id') {
                                var node = document.createElement("Div");
                                div.appendChild(node);
                                node.innerHTML = "<b>" + x[j].nodeName + ":</b>&nbsp;&nbsp;" + x[j].innerHTML;
                                obj[x[j].nodeName] = x[j].innerHTML;
                            }

                        }
                        var hr = document.createElement("hr");
                        div.appendChild(hr);
                    }

                    if (callback) {
                        callback(obj);
                    }
                }
            }
            http.send(params);

        }

        function findAllCompany() {
            callQuery("<find format='all'><Company><Id ne=''/></Company></find>", "all", function (rs) {
                console.log(rs);
            });
        }

        $timeout(function () {
            findAllCompany();
        });
    });
