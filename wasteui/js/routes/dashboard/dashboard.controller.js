'use strict';

angular.module('sampleUIApp')
    .controller('DashboardCtrl', function ($scope, $location, $timeout, QueryService) {

        var vm = this;

        vm.InstrumentTrashBin = InstrumentTrashBin;
        vm.InstrumentIbeaconTrashBinAll = InstrumentIbeaconTrashBinAll;
        vm.InstrumentRfTrashBinAll = InstrumentRfTrashBinAll;
        vm.InstrumentRfTrashBin = InstrumentRfTrashBin;
        vm.InstrumentStreetPost = InstrumentStreetPost;
        vm.InstrumentStreetPostAll = InstrumentStreetPostAll;
        vm.InstrumentOdorSensor = InstrumentOdorSensor;
        vm.InstrumentOdorSensorAll = InstrumentOdorSensorAll;
        vm.SetInfluencer = SetInfluencer;
        vm.findAllCompany = findAllCompany;
        vm.SetWorkerRoot = SetWorkerRoot;
        vm.turnOnCampain = turnOnCampain;
        vm.turnOffCampain = turnOffCampain;

        function postQuery(param) {

            var http = new XMLHttpRequest();
            var url = TQL_URL;
            //var params = "<Atomiton.TqlInterface.InstrumentTrashBin><BinId>Bin[14]</BinId><SensorType>IBeacon</SensorType></Atomiton.TqlInterface.InstrumentTrashBin>";
            http.open("POST", url, true);

            //Send the proper header information along with the request
            http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            // http.setRequestHeader("Content-length", param.length);
            // http.setRequestHeader("Connection", "close");

            http.onreadystatechange = function () {//Call a function when the state changes.
                if (http.readyState == 4 && http.status == 200) {
                    console.log(http.responseText);
                }
            }
            http.send(param);

        }


        function InstrumentTrashBin(id) {

            postQuery("<Atomiton.TqlInterface.InstrumentTrashBin><BinId>" + id + "</BinId><SensorType>IBeacon</SensorType></Atomiton.TqlInterface.InstrumentTrashBin>");
        }

        function InstrumentIbeaconTrashBinAll() {

            postQuery("<Atomiton.TqlInterface.InstrumentTrashBin><BinId ne=''/><SensorType>IBeacon</SensorType></Atomiton.TqlInterface.InstrumentTrashBin>");
        }

        function InstrumentRfTrashBinAll() {

            postQuery("<Atomiton.TqlInterface.InstrumentTrashBin><BinId ne=''/><SensorType>RF</SensorType></Atomiton.TqlInterface.InstrumentTrashBin>");
        }

        function InstrumentRfTrashBin(id) {
            postQuery("<Atomiton.TqlInterface.InstrumentTrashBin><BinId>" + id + "</BinId><SensorType>RF</SensorType></Atomiton.TqlInterface.InstrumentTrashBin>");
        }

        function InstrumentStreetPost(id) {
            postQuery("<Atomiton.TqlInterface.InstrumentStreetPole><PoleId>" + id + "</PoleId><SensorType>IBeacon</SensorType></Atomiton.TqlInterface.InstrumentStreetPole>");
        }

        function InstrumentStreetPostAll() {
            postQuery("<Atomiton.TqlInterface.InstrumentStreetPole><PoleId ne=\"\"/><SensorType>IBeacon</SensorType></Atomiton.TqlInterface.InstrumentStreetPole>");
        }

        function InstrumentOdorSensor(id) {
            postQuery("<Atomiton.TqlInterface.InstrumentStreetPole><PoleId>" + id + "</PoleId><SensorType>Odor</SensorType></Atomiton.TqlInterface.InstrumentStreetPole>");
        }

        function InstrumentOdorSensorAll() {
            postQuery("<Atomiton.TqlInterface.InstrumentStreetPole><PoleId ne=\"\"/><SensorType>Odor</SensorType></Atomiton.TqlInterface.InstrumentStreetPole>");
        }

        function SetInfluencer(id) {
            postQuery("<Atomiton.TqlInterface.SetInfluencer><Id>" + id + "</Id><State>True</State></Atomiton.TqlInterface.SetInfluencer>");
        }

        function findAllCompany() {
            callQuery("<find format='all'><Company><Id ne=''/></Company></find>", "all");
        }

        function SetWorkerRoot(id, dumpsite) {

            postQuery("<SetWorkerRoute><Id>" + id + "</Id><Site>" + dumpsite + "</Site></SetWorkerRoute>");
        }

        function turnOnCampain() {

            postQuery("<Atomiton.TqlInterface.SetCampaignStatus><IsActive>true</IsActive></Atomiton.TqlInterface.SetCampaignStatus>");
        }

        function turnOffCampain() {

            postQuery("<Atomiton.TqlInterface.SetCampaignStatus><IsActive>false</IsActive></Atomiton.TqlInterface.SetCampaignStatus>");
        }


    });
