'use strict';

angular.module('sampleUIApp')
    .controller('PostsCtrl', function ($scope, $location, $timeout, QueryService) {

        var vm = this;

        vm.findAllResident = function () {
            vm.items = [];
            findAllResident(function (rs) {
                console.log(rs);
                $timeout(function () {
                    vm.items = rs;
                })
            });
        }
        vm.findTrashBin = function () {
            vm.itemsTrashBin = [];
            findTrashBin(function (rs) {
                console.log(rs);
                $timeout(function () {
                    vm.itemsTrashBin = rs;
                })
            });
        };
        vm.findAllStreetPole = findAllStreetPole;

        var broken = false;

        function callQuery(params, call, callback) {

            var http = new XMLHttpRequest();
            var url = TQL_URL;
            http.open("POST", url, true);
            http.setRequestHeader("Content-Type", "text/xml");
            //Send the proper header information along with the request

            //http.setRequestHeader("Content-length", params.length);
            //http.setRequestHeader("Connection", "close");

            http.onreadystatechange = function () {//Call a function when the state changes.
                if (http.readyState == 4 && http.status == 200) {
                    //alert(http.responseText);
                    // var div = document.getElementById("dd");


                    // div.innerHTML = "";
                    var parser = new DOMParser();
                    var xmlDoc = parser.parseFromString(http.responseText, "text/xml");
                    var jsonDoc = JSON.parse(xml2json(xmlDoc, ""));
                    var streetPost_arr = _.pluck(jsonDoc.Find.Result, "StreetPole");
                    var array = [];

                    for (var k = 0; k < streetPost_arr.length; k++) {
                        var streetPostId = streetPost_arr[k].Id;
                        var odor = streetPost_arr[k].HasOdor["@Value"];
                        var has_odor = "";
                        var has_ibeacon = "";
                        if (odor == "false") {
                            has_odor = "No";
                        } else {
                            has_odor = "yes";
                        }

                        var ibeacon = streetPost_arr[k].HasIBeacon["@Value"];

                        if (ibeacon == "false") {
                            has_ibeacon = "No";
                        } else {
                            has_ibeacon = "yes";
                        }

                        array.push({
                            has_odor: has_odor,
                            has_ibeacon: has_ibeacon,
                            streetPostId: streetPostId
                        });


                        // var table = document.getElementById("myTable");
                        // var row = table.insertRow(-1);
                        //
                        // var cell1 = row.insertCell(-1);
                        // cell1.innerHTML = streetPostId;
                        //
                        // var cell2 = row.insertCell(1);
                        // cell2.innerHTML = has_odor;
                        //
                        // var cell3 = row.insertCell(2);
                        // cell3.innerHTML = has_ibeacon;


                    }

                    if (callback) {
                        callback(array);
                    }
                }
            }
            http.send(params);

        }

        function findAllResident(callback) {

            callQuery("<find format='all'><StreetPole><Id ne=''/></StreetPole></find>", "all", callback);
        }

        document.getElementById("clock").onload = function () {
            QueryService.WebSocketTest()
        };

        function sendMsg() {
            console.log("message sent");
        }

        function findResidentByID(id) {
            document.getElementById("currentPedestrian").innerHTML = id;
            callQuery("<find format='all'><Resident><Id eq='" + id + "'/></Resident></find>", "getCurrent");
        }

        function callQuery1(params, call, callback) {

            var http = new XMLHttpRequest();
            var url = TQL_URL;
            http.open("POST", url, true);
            http.setRequestHeader("Content-Type", "text/xml");
            //Send the proper header information along with the request

            //http.setRequestHeader("Content-length", params.length);
            //http.setRequestHeader("Connection", "close");

            http.onreadystatechange = function () {//Call a function when the state changes.
                if (http.readyState == 4 && http.status == 200) {
                    //alert(http.responseText);
                    // var div = document.getElementById("dd");


                    // div.innerHTML = "";
                    var parser = new DOMParser();
                    var xmlDoc = parser.parseFromString(http.responseText, "text/xml");
                    var jsonDoc = JSON.parse(xml2json(xmlDoc, ""));
                    var trashbin_arr = _.pluck(jsonDoc.Find.Result, "TrashBin");
                    var array = [];

                    for (var k = 0; k < trashbin_arr.length; k++) {
                        var trashBinId = trashbin_arr[k].Id;
                        var iBeacon = trashbin_arr[k].HasIBeacon["@Value"];

                        if (iBeacon == "false") {
                            var has_ibeacon = "No";
                        } else {
                            var has_ibeacon = "yes";
                        }

                        var capacity = trashbin_arr[k].Capacity["@Value"];
                        var cashInsentive = trashbin_arr[k].CashIncentive["@Value"];
                        var hasRF = trashbin_arr[k].HasRF["@Value"];

                        if (hasRF == "false") {
                            var has_RF = "No";
                        } else {
                            var has_RF = "yes";
                        }

                        array.push({
                            trashBinId: trashBinId,
                            has_ibeacon: has_ibeacon,
                            capacity: capacity,
                            cashInsentive: cashInsentive,
                            hasRF: hasRF
                        })


                        // var table = document.getElementById("myTable");
                        // var row = table.insertRow(-1);
                        //
                        // var cell1 = row.insertCell(-1);
                        // cell1.innerHTML = trashBinId;
                        //
                        // var cell2 = row.insertCell(1);
                        // cell2.innerHTML = has_ibeacon;
                        //
                        // var cell3 = row.insertCell(2);
                        // cell3.innerHTML = capacity;
                        //
                        // var cell4 = row.insertCell(3);
                        // cell4.innerHTML = cashInsentive;
                        //
                        // var cell5 = row.insertCell(4);
                        // cell5.innerHTML = has_RF;


                    }

                    if (callback) {
                        callback(array);
                    }
                }
            }
            http.send(params);

        }


        function findAllStreetPole() {
            callQuery("<find format='all'><StreetPole><Id ne=''/></StreetPole></find>", "all");
        }

        function findTrashBin(callback) {
            callQuery1("<find format='all'><TrashBin><Id ne=''/></TrashBin></find>", "all", callback);
        }


        $timeout(function () {
            vm.findAllResident();
            vm.findTrashBin();
            vm.findAllStreetPole();
        });
    });
