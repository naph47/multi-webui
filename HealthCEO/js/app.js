angular
    .module('sampleUIApp', [
        'ngAnimate',
        'ngCookies',
        'ngResource',
        'ngRoute',
        'ngSanitize',
        'ngTouch',
        'ngMaterial',
        'ngMessages',
        'ui.codemirror',
        'duScroll',
        'angular-loading-bar'
    ])
    .config(function ($routeProvider, cfpLoadingBarProvider) {

        cfpLoadingBarProvider.includeSpinner = false;


        $routeProvider
            .when('/', {
                redirectTo: '/home'
            })
            .when('/home', {
                templateUrl: 'js/routes/home/home.html',
                controller: 'HomeCtrl',
                controllerAs: 'vm',
                resolve: {}
            })
            .when('/account', {
                templateUrl: 'js/routes/account/account.html',
                controller: 'AccountCtrl',
                controllerAs: 'vm',
                resolve: {}
            })
            .when('/education/what', {
                templateUrl: 'js/routes/education.what/education.what.html',
                resolve: {}
            })
            .when('/education/ckd', {
                templateUrl: 'js/routes/education.ckd/education.ckd.html',
                resolve: {}
            })
            .when('/education/cause', {
                templateUrl: 'js/routes/education.cause/education.cause.html',
                resolve: {}
            })
            .when('/gfr_calculator', {
                templateUrl: 'js/routes/gfr_calculator/gfr_calculator.html',
                controller: 'GFRCalculatorCtrl',
                controllerAs: 'vm',
                resolve: {}
            })
            .otherwise({
                redirectTo: '/'
            });
    });
