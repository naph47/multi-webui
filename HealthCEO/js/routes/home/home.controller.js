'use strict';

angular.module('sampleUIApp')
    .controller('HomeCtrl', function ($rootScope, $scope, $location, $timeout, cfpLoadingBar) {
        var vm = this;

        vm.login = function () {
            var user = {
                username: vm.username,
                password: vm.password
            };
            $rootScope.currentUser = user;
            localStorage.setItem("current-user", JSON.stringify(user));
            $('html').scrollTop(0);
            $location.path("/account");
        }


    });
