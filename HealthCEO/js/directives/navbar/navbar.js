'use strict';

/**
 * @ngdoc directive
 * @name greenhouseuiApp.directive:navBar/navBar
 * @description
 * # navBar/navBar
 */
angular.module('sampleUIApp')
    .directive('navBarTop', function ($location, $timeout, $rootScope) {
        return {
            templateUrl: "./js/directives/navbar/navbar.html",
            restrict: 'E',
            link: function postLink(scope, element, attrs) {
                scope.isState = function (name) {
                    return name == $location.path();
                }

                scope.goTo = function (name) {
                    $location.path(name);
                }

                scope.logout = function () {
                    $rootScope.currentUser = null;
                    localStorage.setItem("current-user", null);
                    $location.path("/home");
                }

                function checkLogin() {
                    if (localStorage.getItem("current-user")) {
                        $rootScope.currentUser = JSON.parse(localStorage.getItem("current-user"));
                    }
                }

                $timeout(function () {
                    checkLogin();

                })
            }
        };
    });
