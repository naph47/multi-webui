angular
    .module('sampleUIApp', [
        'ngAnimate',
        'ngCookies',
        'ngResource',
        'ngRoute',
        'ngSanitize',
        'ngTouch',
        'ngMaterial',
        'ngMessages',
        'ui.codemirror',
        'duScroll',
        'angular-loading-bar'
    ])
    .config(function ($routeProvider, cfpLoadingBarProvider) {

        cfpLoadingBarProvider.includeSpinner = false;


        $routeProvider
            .when('/', {
                redirectTo: '/home'
            })
            .when('/home', {
                templateUrl: 'js/routes/home/home.html',
                controller: 'HomeCtrl',
                controllerAs: 'vm',
                resolve: {}
            })
            .when('/about', {
                templateUrl: 'js/routes/about/about.html',
                controllerAs: 'vm',
                resolve: {}
            })
            .when('/calculator', {
                templateUrl: 'js/routes/calculator/calculator.html',
                controllerAs: 'vm',
                resolve: {}
            })
            .otherwise({
                redirectTo: '/'
            });
    });
