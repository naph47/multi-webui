'use strict';

/**
 * @ngdoc directive
 * @name greenhouseuiApp.directive:navBar/navBar
 * @description
 * # navBar/navBar
 */
angular.module('sampleUIApp')
    .directive('navBar', function ($location, $timeout, $rootScope, $mdSidenav) {
        return {
            templateUrl: "./js/directives/navbar/navbar.html",
            restrict: 'E',
            link: function postLink(scope, element, attrs) {
                scope.toggleLeft = function () {
                    console.log("toggle");
                    $mdSidenav('left')
                        .toggle();
                }


            }
        };
    });
