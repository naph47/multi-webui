Stage 5 Chronic Kidney Disease

Protein

- Red meats (beef, veal, lamb)
- Pork
- Poultry (chicken and turkey)
- Fish and other seafood
- Eggs
- Vegetables and grains

Calories

- If you are not getting enough calories from your diet, you may need to eat extra sweets like sugar, jam, jelly, hard candy, honey and syrup, unless you also have diabetes

- Soft (tub) margarine and oils like canola or olive oil

Vitamins or minerals

Glucose
