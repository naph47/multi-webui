# README #

This README would normally document whatever steps are necessary to get your application up and running.

## Symptom Checker ##
location `/symptom-checker/`

### Install ###
* Require nodejs
* install plugins
	* `npm install -g yarn`
	* `yarn install`

### Settings ###
* change app-id, app-key
```
const settings = {
  'app-id': 'your-id-here',
  'app-key': 'your-key-here'
};

export default settings;
```
### Test ###
`yarn dev`

### Build ###
`yarn build`

### Deploy ###
* copy everything into server
* start with url such as [http://localhost:8888/multi-webui/symptom-checker/]